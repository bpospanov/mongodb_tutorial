const assert = require('assert')
const MarioChar = require('../models/mariochar')

// describe tests
describe('Updating records', () => {

    var char;

    beforeEach((done) => {
        char = new MarioChar({
            name: 'Mario',
            weight: 50
        })

        char.save().then(() => {
            done();
        })
    })

    it('Updates one record in the db', (done) => {
        MarioChar.findOneAndUpdate({ name: 'Mario'}, { name: 'Luigi'}, { useFindAndModify: false }).then(() => {
            MarioChar.findOne({ _id: char._id }).then((result) => {
                assert(result.name === 'Luigi')
                done()
            })
        })
    })

    it('Increments the weight by one', (done) => {
        MarioChar.updateMany({}, { $inc: { weight: 1 } }).then(() => {
            MarioChar.findOne({ name: 'Mario'}).then((record) => {
                assert(record.weight === 51)
                done()
            })
        })
    })
})