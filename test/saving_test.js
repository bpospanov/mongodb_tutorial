const assert = require('assert')
const MarioChar = require('../models/mariochar')

// describe tests
describe('Saving records', () => {
    // create test
    it('Saves a record to the db', (done) => {
        var char = new MarioChar({
            name: 'Mario'
        })

        char.save().then(() => {
            assert(char.isNew === false)
            done();
        })
        
        
    })
})