const mongoose = require('mongoose')

// connect to db before test
before((done) => {
    mongoose.connect('mongodb://localhost/test', { useNewUrlParser: true, useUnifiedTopology: true })

    mongoose.connection.once('open', () => {
        console.log('Connection to db has been made')
        done()
    }).on('error', (error) => {
        console.log('Connection error: ' + error)
    })
})

// drop the characters collection before each test
beforeEach((done) => {
    mongoose.connection.collections.mariochars.drop(() => {
        done()
    })
})
